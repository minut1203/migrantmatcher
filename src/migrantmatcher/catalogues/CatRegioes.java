package migrantmatcher.catalogues;

import java.util.ArrayList;
import java.util.List;

import migrantmatcher.Observer.EventManager;
import migrantmatcher.domain.Regiao;

public class CatRegioes {
	List<Regiao> listaRegioes = new ArrayList<>();
	List<Regiao> regioesComAjuda = new ArrayList<>();
	
	public EventManager manager = new EventManager();
	
	
	public void addRegiao(String nome) {
		listaRegioes.add(new Regiao(nome));
	}
	
	public Regiao getRegiao(String nome) {
		for(Regiao regiao : listaRegioes) {
			if(regiao.getNome().equals(nome))
				return regiao;
		}
		return null;
	}
	
	public void addRegiaoComAjuda(String nome) {
		regioesComAjuda.add(new Regiao(nome));
	}
	
	public void printListaRegioes() {
		System.out.print("Lista de Regi�es dispon�veis\n");
		
		for(Regiao r : listaRegioes)
			System.out.println(r.toString());
	}
	
	public List<Regiao> getListaRegioes(){
		return listaRegioes;
	}
	
	public void addRegiaoComAjuda(Regiao regiao) {
		regioesComAjuda.add(regiao);
		manager.notify(regiao);
	}
	
	public void printListaRegioesComAjuda() {
		System.out.print("Lista de Regi�es com Ajudas\n");
		
		for(Regiao r : regioesComAjuda)
			System.out.println(r.toString());
	}
	
	public int size() {
		return listaRegioes.size();
	}
}
