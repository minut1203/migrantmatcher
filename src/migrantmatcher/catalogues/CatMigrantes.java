package migrantmatcher.catalogues;

import java.util.ArrayList;
import java.util.List;

import migrantmatcher.domain.Regiao;
import migrantmatcher.domain.migrantes.Familia;
import migrantmatcher.domain.migrantes.Individuo;
import migrantmatcher.domain.migrantes.MembroFamilia;
import migrantmatcher.domain.migrantes.Migrante;

public class CatMigrantes {
	
	List<Migrante> listaIndividuos = new ArrayList<>();
	List<Familia> familias = new ArrayList<>();
	//devia fazer uma lista de individuos e uma lista de familias

	
	//arranjar problema de return
	public Migrante getMigrante(String nome) {
		for(Migrante m : listaIndividuos) {
			if(m.getNome().equals(nome))
				return m;
		}
		return null;
	}
	
	public void addIndividuo(Migrante migranteAtual) {
		listaIndividuos.add(migranteAtual);
	}
	
	public void newFamilia(Familia familia) {
		familias.add(familia);
	}
	
	public void addCabeca(Familia familia, Individuo cabeca) {
		for(Familia fam : familias) {
			if(fam.equals(familia))
				fam.addCabeca(cabeca);
		}
	}
	
	public void addMembro(Familia familia, MembroFamilia m) {
		for(Familia fam : familias) {
			if(fam.equals(familia))
				fam.addMembro(m);
		}
	}
	
	//verificar como meter um return
	public Familia getFamilia(List<Migrante> familia){
		for(Familia fam : familias) {
			if(fam.equals(familia))
				return fam;
		}
		return null;
	}
}
