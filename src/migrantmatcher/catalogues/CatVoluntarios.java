package migrantmatcher.catalogues;

import java.util.ArrayList;
import java.util.List;

import migrantmatcher.domain.Voluntario;
import migrantmatcher.domain.SMS.SMS;

public class CatVoluntarios {
	List<Voluntario> listaVoluntarios = new ArrayList<>();
	
	public Voluntario addVoluntario(Voluntario v) {
		for(Voluntario vol : listaVoluntarios) {
			if(vol.equals(v))
				return vol;
		}
		listaVoluntarios.add(v);
		return v;
	}
	
	public int size() {
		return listaVoluntarios.size();
	}
	
	
}
