package migrantmatcher.catalogues;

import java.util.ArrayList;
import java.util.List;

import migrantmatcher.domain.Voluntario;
import migrantmatcher.domain.ajudas.AjudasNode;
import migrantmatcher.domain.ajudas.Alojamento;
import migrantmatcher.domain.ajudas.Item;

public class CatAjudas{
	AjudasNode listaAjudas = new AjudasNode();
	AjudasNode listaSelecionadas = new AjudasNode();
	
	public AjudasNode getListaAjudasSelecionadas() {
		return this.listaSelecionadas;
	}
	
	public void addAlojamento(Alojamento al) {
		listaAjudas.addAlojamento(al);
	}
	
	public void addItem(Item it) {
		listaAjudas.addItem(it);
	}
	
	public void addAlojamentoSelecionado(int ID) {
		Alojamento al = listaAjudas.getAlojamento(ID);
		if(al==null)
			System.out.print("Ocorreu um erro: Alojamento n�o existe.");
		else
			listaSelecionadas.addAlojamento(al);
	}
	
	public void addItemSelecionado(int ID) {
		Item it = listaAjudas.getItem(ID);
		if(it == null)
			System.out.print("Ocorreu um erro: Item n�o existe.");
		else
			listaSelecionadas.addItem(it);
	}
	
	public List<Voluntario> getVoluntariosAjudasSelecionadas(){
		List<Voluntario> listaVolunt = new ArrayList<>();
		for(Alojamento al : listaSelecionadas.getAlojamentos())
			listaVolunt.add(al.getVoluntario());
		for(Item it : listaSelecionadas.getItems())
			listaVolunt.add(it.getVoluntario());
		return listaVolunt;
	}
	
	public List<Integer> printAjudasEmRegiao(String regiao) {
		List<Integer> IDsRegiao = new ArrayList<>();
		
		if(listaAjudas.getAlojamentos(regiao).size() > 0){
			System.out.println("Ajudas em " + regiao);
			System.out.println("Alojamentos\n");
			for(Alojamento al : listaAjudas.getAlojamentos(regiao)) {
				System.out.println(al.toString());
				IDsRegiao.add(al.getID());
			}
			System.out.println("Items\n");
			for(Item it : listaAjudas.getItems()) {
				System.out.print(it.toString());
				IDsRegiao.add(it.getID());
			}
		}
		return IDsRegiao;
	}
	
	public int size() {
		return listaAjudas.numAjudas();
	}
}
