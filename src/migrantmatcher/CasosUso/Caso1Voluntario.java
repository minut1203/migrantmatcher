package migrantmatcher.CasosUso;

import java.util.Scanner;

import migrantmatcher.handlers.RegistarAjudaHandler;

public class Caso1Voluntario {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		RegistarAjudaHandler handler = new RegistarAjudaHandler();
		
		handler.inicializacao();

		System.out.print("Boas Volunt�rio!\nIndique o seu contacto:\n");
		handler.registar(sc.nextInt());

		System.out.print("Que tipo de ajuda pretende oferecer?(Alojamento ou Item)\n");
		String tipoAjuda = sc.next();

		boolean valorCorreto = false;// tb podia usar breaks mas mais vale um bool
		while (!valorCorreto) {
			if (tipoAjuda.equalsIgnoreCase("Alojamento")) {
				handler.oferecerAlojamento();
				System.out.print("Insira o n�mero de pessoas que a habita��o alberga.\n");
				handler.indicarNumAlbergados(sc.nextInt());
				sc.nextLine();
				boolean regiaoCorreta = false;
				while (!regiaoCorreta) {
					System.out.print("Indique a regi�o da habita��o\n");
					regiaoCorreta = handler.indicarRegiaoAlojamento(sc.next());
				}
				handler.enviarCodigo();
				valorCorreto = true;
			} else if (tipoAjuda.equalsIgnoreCase("Item")) {
				handler.oferecerItems();
				System.out.print("Indique a descricao do item.\n");
				handler.indicarDescricaoItem(sc.next());
				handler.enviarCodigo();
				valorCorreto = true;
			} else {
				System.out.print("\nErro");
				tipoAjuda = sc.next();
			}
				
		}

		boolean verifCodigo = false;

		while (!verifCodigo) {
			System.out.print("Insira o codigo de confirma��o para confirmar a ajuda.");
			verifCodigo = handler.confirmarAjuda(sc.nextInt());
		}
		handler.testes();
		sc.close();
	}
}
