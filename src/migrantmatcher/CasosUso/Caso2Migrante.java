package migrantmatcher.CasosUso;

import java.util.Scanner;

import migrantmatcher.handlers.ProcurarAjudaHandler;

public class Caso2Migrante {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		ProcurarAjudaHandler handler = new ProcurarAjudaHandler();
		
		handler.inicializacao();
		
		//System.out.print();
		System.out.print("Escolha o tipo de inscri��o:\n"
				+ "1-Indiv�duo\n"
				+ "2-Fam�lia\n");
		int escolha = sc.nextInt();
		sc.nextLine();
		
		if(escolha == 1){
			System.out.print("Indique o seu nome e contacto.");
			handler.registarIndividuo(sc.next(), sc.nextInt());
			sc.nextLine();
			
		}else if(escolha == 2) {
			System.out.print("Indique o n�mero de pessoas da sua fam�lia.");
			handler.registarFamilia(sc.nextInt());
			sc.nextLine();
			
			System.out.print("Insira os dados do cabe�a de fam�lia.(nome e num�ro de telefone)");
			handler.indicarDadosCabeca(sc.next(), sc.nextInt());
			sc.nextLine();
			
			String input = new String();
			while(!input.equalsIgnoreCase("fim")) {
				System.out.print("Insira �fim� para terminar o processo de adicionar membros");
				System.out.print("Indique os dados do membro da fam�lia.");
				input = sc.next();
				if(!input.equalsIgnoreCase("fim"))
					handler.indicarDadosMembro(input);
			}
			
		}
		handler.getListaRegioes();
		
		boolean escolhaValida = false;
		boolean regiaoValida = false;
		
		while(!regiaoValida) {
			System.out.print("Indique a regi�o para a qual se quer mover.");
			String regiao = sc.next();
			if(!handler.indicarRegiao(regiao)) {
				System.out.println("Regi�o n�o possui ajudas de momento. Pretende ser notificado casso passe a ter?");
				if(sc.next().equals("sim"))
					handler.requisitarNotificacao(regiao);
			}else 
				regiaoValida=true;
		}
		
		while(!escolhaValida) {
			System.out.print("Selecione a ajuda que pretende inserindo o tipo de ajuda (alojamento/item) e o ID da ajuda\n");
			escolhaValida = handler.escolherAjuda(sc.next(), sc.nextInt());
			sc.nextLine();
			
			System.out.print("Deseja selecionar mais ajudas?(Sim/N�o)");
			String resposta = sc.next();
			if(resposta.equalsIgnoreCase("nao"))
				escolhaValida = true;
			else if(resposta.equalsIgnoreCase("sim"))
				escolhaValida = false;
			else {
				escolhaValida=false;
				System.out.println("Insira sim ou nao");
			}
		}
		
			
		handler.confirmar();
		System.out.print("Obrigado. Os volunt�rios foram informados.");
	}

}
