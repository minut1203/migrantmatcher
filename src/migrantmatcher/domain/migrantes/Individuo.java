package migrantmatcher.domain.migrantes;

import java.util.Objects;

import migrantmatcher.domain.Regiao;

public class Individuo extends Migrante{
	int contacto;
	Regiao regiaoDestino;
	
	public Individuo(String nome, int num_tel) {
		this.nome = nome;
		this.contacto = num_tel;
	}
	
	public int getContacto() {
		return this.contacto;
	}
	
	public Regiao getRegiaoDestino() {
		return this.regiaoDestino;
	}
	
	public void setRegiaoDestino(String nome) {
		regiaoDestino = new Regiao(nome);
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(contacto);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Individuo other = (Individuo) obj;
		return contacto == other.contacto;
	}
	
}
