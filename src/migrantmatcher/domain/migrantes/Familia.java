package migrantmatcher.domain.migrantes;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Familia{
	Individuo cabeca;
	List<MembroFamilia> familia;
	
	public Familia(int num_pessoas) {
		familia = new ArrayList<>(num_pessoas);
	}
	
	public void addCabeca(Individuo cabeca) {
		this.cabeca = cabeca;
	}
	
	public void addMembro(MembroFamilia mf) {
		familia.add(mf);
	}
	
	public Individuo getCabeca() {
		return this.cabeca;
	}
	
	public MembroFamilia getMembro(String nome) {
		for(MembroFamilia membro : familia) {
			if(membro.getNome().equals(nome))
				return membro;
		}
		return null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cabeca);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Familia other = (Familia) obj;
		return Objects.equals(cabeca, other.cabeca);
	}
	
}
