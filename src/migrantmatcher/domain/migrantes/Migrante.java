package migrantmatcher.domain.migrantes;

import java.util.Objects;

import migrantmatcher.domain.Regiao;
import migrantmatcher.domain.ajudas.AjudasNode;

public class Migrante {
	String nome;
	AjudasNode listaAjudasPedidas;
	Regiao regiaoRequisitada;
	
	public String getNome() {
		return this.nome;
	}
	
	public void setAjudasPedidas(AjudasNode ajPedidas) {
		listaAjudasPedidas = ajPedidas;
	}
	
	public void addRegiaoRequisitada(Regiao regiao) {
		regiaoRequisitada = regiao;
	}
	
	public Regiao getRegiaoRequisitada(){
		return regiaoRequisitada;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(nome);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Migrante other = (Migrante) obj;
		return Objects.equals(nome, other.nome);
	}
}
