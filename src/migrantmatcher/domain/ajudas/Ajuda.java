package migrantmatcher.domain.ajudas;

import java.util.Objects;

import migrantmatcher.domain.Voluntario;

public abstract class Ajuda{
	
	protected Voluntario voluntario;
	protected int ID;
	
	protected Ajuda(Voluntario v, int ID) {
		this.voluntario = v;
		this.ID = ID;
	}
	
	public Voluntario getVoluntario() {
		return this.voluntario;
	}
	
	public int getID() {
		return this.ID;
	}

	@Override
	public int hashCode() {
		return Objects.hash(voluntario);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ajuda other = (Ajuda) obj;
		return Objects.equals(voluntario, other.voluntario);
	}
}
