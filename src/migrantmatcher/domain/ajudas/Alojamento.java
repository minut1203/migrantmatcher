package migrantmatcher.domain.ajudas;

import java.util.Objects;

import migrantmatcher.domain.Regiao;
import migrantmatcher.domain.Voluntario;

public class Alojamento extends Ajuda{
	
	int numAlbergados;
	Regiao regiao;
	
	public Alojamento(Voluntario v, int ID) {
		super(v, ID);
	}
	
	public void setNumAlbergados(int num_albergados) {
		this.numAlbergados = num_albergados;
	}
	
	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	
	public Regiao getRegiao() {
		return regiao;
	}
	
	@Override
	public String toString() {
		return "Alojamento " + this.ID + " Albergados:" + numAlbergados + "\nRegiao:" + regiao.toString() + "\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(numAlbergados, regiao);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alojamento other = (Alojamento) obj;
		return numAlbergados == other.numAlbergados && Objects.equals(regiao, other.regiao);
	}

	
	
}
