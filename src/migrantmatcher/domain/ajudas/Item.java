package migrantmatcher.domain.ajudas;

import java.util.Objects;

import migrantmatcher.domain.Voluntario;

public class Item extends Ajuda{

	String descricao;
	
	public Item(Voluntario v, int ID) {
		super(v, ID);
	}

	public void setDescricao(String descricao){
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(descricao);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		return Objects.equals(descricao, other.descricao);
	}

	@Override
	public String toString() {
		return "Item " + this.ID + " Descri��o " + descricao + "\n";
	}


}
