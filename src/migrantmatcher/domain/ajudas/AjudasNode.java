package migrantmatcher.domain.ajudas;

import java.util.ArrayList;
import java.util.List;

public class AjudasNode {
	List<Alojamento> alojamentos = new ArrayList<>();
	List<Item> items = new ArrayList<>();

	public void addAlojamento(Alojamento al) {
		alojamentos.add(al);
	}

	public void addItem(Item it) {
		items.add(it);
	}
	
	public Alojamento getAlojamento(int ID) {
		for(Alojamento al : alojamentos)
			if(al.getID()==ID)
				return al;
		return null;
	}
	
	public Item getItem(int ID) {
		for(Item it : items)
			if(it.getID()==ID)
				return it;
		return null;
	}
	
	public List<Alojamento> getAlojamentos(String regiao) {
		List<Alojamento> aljNaRegiao = new ArrayList<>();
		for (Alojamento al : alojamentos) {
			if (al.getRegiao().getNome().equals(regiao))
				aljNaRegiao.add(al);
		}
		return aljNaRegiao;
	}
		
	public List<Alojamento> getAlojamentos(){
		return alojamentos;
	}
	
	public List<Item> getItems() {
		return items;
	}

	public int numAjudas() {
		return alojamentos.size() + items.size();
	}

	// public List? ordenar();
}
