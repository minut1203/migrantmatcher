package migrantmatcher.domain.SMS;

import com.pidgeonsmssender.sdk.PidgeonSMSSender;

public class PidgeonSMS implements SMSSender{
	PidgeonSMSSender pSender = new PidgeonSMSSender();

	public void enviarMensagem(int num, String mensagem) {
		pSender.send(Integer.toString(num), mensagem);
	}
}
