package migrantmatcher.domain.SMS;

import java.util.List;

public class SMS {
	
	protected SMSSender sms;
	
	public SMS(SMSSender sms) {
		this.sms = sms;
	}
	
	public void enviarCodigo(int num_tel, int codigo) {
		sms.enviarMensagem(num_tel, Integer.toString(codigo));
	}
	
	public void notificarVoluntarios(List<Integer> contactos, int contactoMigrante, String nomeMigrante) {
		for(int num_tel : contactos)
			sms.enviarMensagem(num_tel, "Ajuda requisitada por migrante: " + nomeMigrante + " \tContacto: " + contactoMigrante);
	}
	
	public void notificarMigrante(int contactoMigrante, String regiao) {
		sms.enviarMensagem(contactoMigrante, "Regi�o " + regiao + " ja tem ajuda dispon�vel.");
	}
}
