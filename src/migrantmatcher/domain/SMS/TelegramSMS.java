package migrantmatcher.domain.SMS;

import com.telegramsms.TelegramSMSSender;

public class TelegramSMS implements SMSSender{
	TelegramSMSSender tSender = new TelegramSMSSender();
	
	public void enviarMensagem(int num, String mensagem) {
		tSender.setNumber(Integer.toString(num));
		tSender.setText(mensagem);
		tSender.send();
	}
}
