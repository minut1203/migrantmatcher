package migrantmatcher.domain;

import java.util.Objects;

public class Voluntario {
	int num_tel;
	
	public Voluntario(int num_tel) {
		this.num_tel = num_tel;
	}
	
	public int getContacto() {
		return this.num_tel;
	}

	@Override
	public int hashCode() {
		return Objects.hash(num_tel);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Voluntario other = (Voluntario) obj;
		return num_tel == other.num_tel;
	}

	@Override
	public String toString() {
		return "Voluntario: " + num_tel;
	}
	
	
}
