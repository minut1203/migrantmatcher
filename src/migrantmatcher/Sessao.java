package migrantmatcher;

import migrantmatcher.handlers.*;

public interface Sessao {
	
	public ProcurarAjudaHandler getProcurarAjudaHandler();
	
	public RegistarAjudaHandler getRegistarAjudaHandler();

}
