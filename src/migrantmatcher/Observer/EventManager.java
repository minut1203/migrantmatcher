package migrantmatcher.Observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import migrantmatcher.domain.Regiao;

public class EventManager {
	Map<Regiao, List<EventListener>> listeners = new HashMap<>();                                                                                 
	
	public void newRegiaoEvent(Regiao regiao) {
		this.listeners.put(regiao, new ArrayList<>());
	}
	
	public void associarMigrante(Regiao regiao, EventListener listener) {
		List<EventListener> users = listeners.get(regiao);
		users.add(listener);
	}
	
	public void desassociarMigrante(Regiao regiao, EventListener listener) {
		List<EventListener> users = listeners.get(regiao);
		users.remove(listener);
	}
	
	public void notify(Regiao regiao) {
		List<EventListener> users = listeners.get(regiao);
		for(EventListener listener : users) {
			listener.update(regiao.getNome());
		}
	}
}
