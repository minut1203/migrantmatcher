package migrantmatcher.Observer;

import migrantmatcher.domain.SMS.PidgeonSMS;
import migrantmatcher.domain.SMS.SMS;
import migrantmatcher.domain.SMS.TelegramSMS;
import migrantmatcher.domain.migrantes.Individuo;

public class MigrantNotificationListener implements EventListener{

	private Individuo migrante;
	
	public MigrantNotificationListener(Individuo migrante) {
		this.migrante = migrante;
	}
	
	@Override
	public void update(String regiao) {
		SMS smsTelegram = new SMS(new TelegramSMS());
		SMS smsPidgeon = new SMS(new PidgeonSMS());
		
		smsTelegram.notificarMigrante(migrante.getContacto(), regiao);
		smsPidgeon.notificarMigrante(migrante.getContacto(), regiao);
	}
	
}
