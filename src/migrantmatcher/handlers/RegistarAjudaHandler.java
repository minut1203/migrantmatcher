package migrantmatcher.handlers;
import java.util.List;
import migrantmatcher.domain.*;
import migrantmatcher.domain.SMS.*;
import migrantmatcher.domain.ajudas.Ajuda;
import migrantmatcher.domain.ajudas.Alojamento;
import migrantmatcher.domain.ajudas.Item;
import migrantmatcher.catalogues.*;

public class RegistarAjudaHandler {
	
	int ajudaIDCounter=0;
	Ajuda ajudaAtual;
	Voluntario vAtual;
	int codigoVerificacao;
	CatAjudas catAjudas = new CatAjudas();
	CatVoluntarios catVoluntarios = new CatVoluntarios();
	CatRegioes catRegioes = new CatRegioes();
	
	/*
	 * Regista voluntario com num_tel caso este voluntario nao exista ja.
	 * Se existir, retorna o voluntario associado a esse numero telefonico
	 */
	public void registar(int num_tel) {	
		vAtual = catVoluntarios.addVoluntario(new Voluntario(num_tel));
	}
	
	/*
	 * Cria alojamento e associa voluntario que ofereceu a esse alojamento
	 */
	public void oferecerAlojamento() {
		ajudaAtual = new Alojamento(vAtual, ajudaIDCounter);
		ajudaIDCounter++;
		catAjudas.addAlojamento((Alojamento) ajudaAtual);
	}
	
	/*
	 * lotacao maxima do alojamento oferecido
	 * devolve lista de regioes pra indicar onde o alojamento se encontra
	 */
	public void indicarNumAlbergados(int num_pessoas){
		Alojamento al = (Alojamento) ajudaAtual;
		al.setNumAlbergados(num_pessoas);
		
		//System.out.print(ajudaAtual.toString());
		
		catRegioes.printListaRegioes();
		
	}
	
	/*
	 * Indica a regiao do alojamento oferecido
	 * cria um SMS associando ao voluntario atual
	 */
	public boolean indicarRegiaoAlojamento(String regiao) {
		Alojamento al = (Alojamento) ajudaAtual;
		
		List<Regiao> list = catRegioes.getListaRegioes();
		for(Regiao r : list)
			if(r.getNome().equalsIgnoreCase(regiao)) {
				al.setRegiao(r);
				catRegioes.addRegiaoComAjuda(r);
				return true;
			}
		System.out.print("Regi�o indispon�vel. Selecione umas das regi�es da lista.\n");
		return false;
	}
	
	/*
	 * cria Item e associa-o ao voluntario
	 */
	public void oferecerItems() {
		ajudaAtual = new Item(vAtual, ajudaIDCounter);
		ajudaIDCounter++;
		catAjudas.addItem((Item) ajudaAtual);
	}
	
	/*
	 * indica a descricao do item 
	 * cria um SMS associando ao voluntario
	 */
	public void indicarDescricaoItem(String descricao) {
		Item it = (Item) ajudaAtual;
		it.setDescricao(descricao);
	}
	
	public void enviarCodigo() {
		SMS smsTelegram = new SMS(new TelegramSMS());
		SMS smsPidgeon = new SMS(new PidgeonSMS());
		
		int min = 100000;
		int max = 999999;
		this.codigoVerificacao = (int) (Math.floor(Math.random() * (max - min)) + min); 
		
		//devolve codigo enviado
		smsTelegram.enviarCodigo(vAtual.getContacto(), codigoVerificacao);
		smsPidgeon.enviarCodigo(vAtual.getContacto(), codigoVerificacao);
	}
	
	/*
	 * verifica se o codigo indicado pelo voluntario foi o enviado
	 */
	public boolean confirmarAjuda(int codigo) {
		if(codigo==codigoVerificacao)
			System.out.print("Ajuda confirmada. Obrigado!!\n");
		else 
			System.out.print("C�digo Errado.\n");

		return codigo==codigoVerificacao;
	}
	
	public int getIDCounter() {
		return this.ajudaIDCounter;
	}
	//////////////////////////////////////////////////
	
	public void inicializacao() {
		catRegioes.addRegiao("Lisboa");
		catRegioes.addRegiao("Porto");
		catRegioes.addRegiao("Leiria");
		catRegioes.addRegiao("Algarve");
	}
	
	public void testes() {

		System.out.println("\nAjuda atual: " + ajudaAtual.toString());
		System.out.println("Voluntario atual " + vAtual.getContacto());
		System.out.println("CatAjudas " + catAjudas.size());
		System.out.println("CatRegioes " + catRegioes.size());
		catRegioes.printListaRegioesComAjuda();
		System.out.println("CatVoluntarios " + catVoluntarios.size());
		
	}
}
