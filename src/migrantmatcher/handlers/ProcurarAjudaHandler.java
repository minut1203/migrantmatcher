package migrantmatcher.handlers;

import migrantmatcher.Observer.EventManager;
import migrantmatcher.Observer.MigrantNotificationListener;
import migrantmatcher.catalogues.CatAjudas;
import migrantmatcher.catalogues.CatMigrantes;
import migrantmatcher.catalogues.CatRegioes;
import migrantmatcher.domain.*;
import migrantmatcher.domain.SMS.PidgeonSMS;
import migrantmatcher.domain.SMS.SMS;
import migrantmatcher.domain.SMS.TelegramSMS;
import migrantmatcher.domain.ajudas.Alojamento;
import migrantmatcher.domain.ajudas.Item;
import migrantmatcher.domain.migrantes.Familia;
import migrantmatcher.domain.migrantes.Individuo;
import migrantmatcher.domain.migrantes.MembroFamilia;
import migrantmatcher.exceptions.RegiaoNaoTemAjudaException;

import java.util.ArrayList;
import java.util.List;

public class ProcurarAjudaHandler {
	
	
	
	Individuo migranteAtual;
	Familia famAtual;
	List<Integer> IDsRegiao;
	CatMigrantes catMigrantes = new CatMigrantes();
	CatAjudas catAjudas = new CatAjudas();
	CatRegioes catRegioes = new CatRegioes();

	/*
	 * Cria novo individuo do tipo Migrante
	 */
	public void registarIndividuo(String nome, int num_tel) {
		migranteAtual = new Individuo(nome, num_tel);
		catMigrantes.addIndividuo(migranteAtual);
	}

	/*
	 * cria uma lista de membros familia onde se vao incluir o cabeca e os membros
	 */
	public void registarFamilia(int num_pessoas) {
		famAtual = new Familia(num_pessoas);
		catMigrantes.newFamilia(famAtual);
	}

	/*
	 * Cria Individuo do tipo Migrante e associa-lhe a lista de membros familia
	 */
	public void indicarDadosCabeca(String nome, int num_tel) {
		migranteAtual = new Individuo(nome, num_tel);
		catMigrantes.addCabeca(famAtual, migranteAtual);
		famAtual.addCabeca(migranteAtual);
	}

	/*
	 * Indica um membro a colocar na lista de membros do cabeca
	 */
	public void indicarDadosMembro(String nome) {
		MembroFamilia m = new MembroFamilia(nome);
		catMigrantes.addMembro(famAtual, m);
		famAtual.addMembro(m);
	}

	/*
	 * devolve a lista de regioes que possuem ajuda para o migrante escolher
	 */
	public void getListaRegioes() {
		catRegioes.printListaRegioesComAjuda();
	}

	/*
	 * associa o migrante � regiao indicada retorna uma lista de ajudas naquela
	 * regiao
	 */

	public boolean indicarRegiao(String regiao) {
		migranteAtual.setRegiaoDestino(regiao);
		IDsRegiao = catAjudas.printAjudasEmRegiao(regiao);	
		return (IDsRegiao == null);
	}

	/*
	 * Exception function associa regiao ao individuo de forma a enviar uma
	 * notificacao quando houver uma ajuda naquela regiao
	 */
	public void requisitarNotificacao(String regiao) {
		Regiao regiaoRequerida = catRegioes.getRegiao(regiao);
		catRegioes.manager.newRegiaoEvent(regiaoRequerida);
		catRegioes.manager.associarMigrante(regiaoRequerida, new MigrantNotificationListener(migranteAtual));
	}

	/*
	 * Escolhe a ajuda pretendida da lista de ajudas
	 */
	public boolean escolherAjuda(String tipoAjuda, int ID) {
		if (IDsRegiao.contains(ID)) {
			if (tipoAjuda.equalsIgnoreCase("alojamento")) {
				catAjudas.addAlojamentoSelecionado(ID);
				return true;
			} else if (tipoAjuda.equalsIgnoreCase("item")) {
				catAjudas.addItemSelecionado(ID);
				return true;
			} else
				System.out.println("Tipo de ajuda n�o existente");

		} else
			System.out.println("ID n�o existe");

		return false;
	}

	/*
	 * cria e envia um SMS ao(s) voluntario(s) relacionado(s) com a(s) ajuda(s)
	 */
	public void confirmar() {
		migranteAtual.setAjudasPedidas(catAjudas.getListaAjudasSelecionadas());

		List<Integer> contactos = new ArrayList<>();
		for (Voluntario v : catAjudas.getVoluntariosAjudasSelecionadas())
			contactos.add(v.getContacto());

		SMS smsTelegram = new SMS(new TelegramSMS());
		SMS smsPidgeon = new SMS(new PidgeonSMS());

		smsTelegram.notificarVoluntarios(contactos, migranteAtual.getNome(), migranteAtual.getContacto());
		smsPidgeon.notificarVoluntarios(contactos, migranteAtual.getNome(), migranteAtual.getContacto());
	}
	
	
	//testes
	
	public void inicializacao() {
		catRegioes.addRegiaoComAjuda("Lisboa");
		catRegioes.addRegiaoComAjuda("Algarve");
		catRegioes.addRegiaoComAjuda("Porto");
		
		Voluntario v1 = new Voluntario(2665);
		Voluntario v2 = new Voluntario(55376);
		Item it1 = new Item(v1, 0);
		it1.setDescricao("cadeira");
		Item it2 = new Item(v2, 2);
		it2.setDescricao("mesa");
		catAjudas.addItem(it1);
		catAjudas.addItem(it2);
		
		Alojamento al1 = new Alojamento(v1, 5);
		al1.setNumAlbergados(4);
		al1.setRegiao(new Regiao("Porto"));
		Alojamento al2 = new Alojamento(v2, 3);
		al2.setNumAlbergados(5);
		al2.setRegiao(new Regiao("Lisboa"));
		
		catAjudas.addAlojamento(al1);
		catAjudas.addAlojamento(al2);
	}
}
